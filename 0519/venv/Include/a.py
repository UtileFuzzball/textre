import tensorflow as tf

hello = tf.constant('hello, TensorFlow!')

with tf.Session() as sess:
    result = sess.run(hello)
    print(result)
