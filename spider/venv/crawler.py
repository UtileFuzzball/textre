__author__ = 'Lesley'
import urllib
import re
import Excelerator
from pathlib import Path


class GooglePlay:
    def __init__(self):
        self.extra = ''
        self.catagories = []
        self.apps = []

    def getPage(self, extra):
        try:
            url = 'https://play.google.com' + extra
            request = urllib.Request(url)
            response = urllib.urlopen(request)
            pageCode = response.read()
            return pageCode
        except urllib.URLError as e:
            print (e.reason)

    def getPage2(self, extra):
        try:
            url = 'apkleecher.com/?id=' + extra
            request = urllib.Request(url)
            response = urllib.urlopen(request)
            pageCode = response.read()
            return pageCode
        except urllib.URLError as e:
            print (e.reason)

    def getDetailsOfApps(self, package):

        pageCode = self.getPage('/store/apps/details?id=' + package)
        if not pageCode:
            print
            "loading error of one app page..."
            return None

        pattern2 = re.compile('<div class="K9wGie"><div class="BHMmbe" aria-label=".*?">(.*?)</div>', re.S)
        items2 = re.findall(pattern2, pageCode)
        pattern1 = re.compile(
            '<a itemprop="genre" href="https://play.google.com/store/apps/category/(.*?)" class="hrTbp R8zArc">.*?</a>',
            re.S)
        items1 = re.findall(pattern1, pageCode)
        pattern3 = re.compile(
            '<div class="JHTxhe"><div class="xyOfqd">.*?<div class="hAyfc"><div class="BgcNfc">(.*?)</div><span class="htlgb"><div><span class="htlgb">(.*?)</span></div></span></div>.*?<div class="hAyfc"><div class="BgcNfc">(.*?)</div><span class="htlgb"><div><span class="htlgb">(.*?)</span></div></span></div>.*?<div class="hAyfc"><div class="BgcNfc">(.*?)</div><span class="htlgb"><div><span class="htlgb">(.*?)</span></div></span></div>.*?<div class="hAyfc"><div class="BgcNfc">(.*?)</div><span class="htlgb"><div><span class="htlgb">(.*?)</span></div></span></div>.*?<div class="hAyfc"><div class="BgcNfc">(.*?)</div><span class="htlgb"><div><span class="htlgb">(.*?)</span></div></span></div>',
            re.S)
        items3 = re.findall(pattern3, pageCode)

        for item1 in items1:
            print
            item1
            self.apps.append(item1)
        if len(items2) == 0:
            self.apps.append('0')
        else:
            self.apps.append(items2)

        for item3 in items3:
            self.apps.append(item3)

    def getDownloadURL(self, appIndex):
        pageCode = self.getPage2(self.apps[appIndex][1])
        if not pageCode:
            print
            "loading error in apkleecher..."
            return None
        print
        getDownloadURL

    def start(self):
        i = 0
        while True:
            path1 = 'C:\\Users\\dell\\Desktop\\GooglePlay\\' + str(i) + '.txt'
            my_file = Path(path1)
            if my_file.is_file():
                file = open(path1, "r")
                while True:
                    line = file.readline()
                    if line:
                        line_array = line.split(",")
                        package = line_array[1].replace("\"", "").replace("\"", "")
                        self.getDetailsOfApps(package)
                    else:
                        i = i + 1
                        break
                for x in self.apps:
                    path2 = "C:\\Users\\dell\\Desktop\\" + str(i) + ".txt"
                    g = open(path2, 'a')
                    temp = '\t'.join(x)
                    txt = temp + '\n'
                    g.write(txt)
                    g.close

            else:
                break


spider = GooglePlay()
spider.start()