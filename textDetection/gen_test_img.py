#!/usr/bin/python3
# --coding:utf-8--

"""
gen_test_img.py 生成测试样本
本模块用来产生测试样本
1. 放大
2. 变亮
3. 变暗
4. 加文字
"""
import random

import numpy as np
import cv2
from PIL import Image


def addImage(path, path_dist, border_x, border_y):
    png = path
    img = Image.open(png)
    info = img.size
    w = info[0]
    h = info[1]
    # print(str(w)+"w是1维"+str(h))
    new_pic = Image.new('RGB', (w + border_x * 2, h + border_y * 2), color="black")
    new_pic.paste(img, (int(border_x), int(border_y)))
    new_pic.save(path_dist)
    print("addimage"+str(new_pic.size))

def randomPoint(points,change_type):
    #放大
    #border_x=50
    #border_y=50
    print("global variables")
    # print(globals())
    if change_type=="large":
        points[0][0] = random.randint(0,border_x)
        points[0][1] = random.randint(0,border_y)
        points[1][0] = random.randint(1280+border_x, 1280+2*border_x)
        points[1][1] = random.randint(0, border_y)

        points[2][0] = random.randint(1280+border_x, 1280+2*border_x)
        points[2][1] = random.randint(720+border_y, 720+border_y*2)
        points[3][0] = random.randint(0, border_x)
        points[3][1] = random.randint(720+border_y,720+2*border_y)
    elif change_type=="small":
        points[0][0] = random.randint(0,border_x)
        points[0][1] = random.randint(0,border_y)
        points[1][0] = random.randint(1280-border_x, 1280)
        points[1][1] = random.randint(0, border_y)

        points[2][0] = random.randint(1280-border_x, 1280)
        points[2][1] = random.randint(720-border_y, 720)
        points[3][0] = random.randint(0, border_x)
        points[3][1] = random.randint(720-border_y,720)

    print(change_type+" random Points: ",)
    print(points)
    # 缩小
    # y轴拉伸
    # x轴拉伸

def perspectiveTransform(path, dis_path, border_x,border_y, list1,list2):
    img = cv2.imread(path)
    img_info = img.shape
    w = img_info[1]
    h = img_info[0]
    #border_x=50
    #border_y=50
    channel = img_info[2]
    print("rows" + str(w) + "cols" + str(h), "channel" + str(channel))
    pst1 = np.float32(list1)#[[border_x, border_y], [w-border_x, border_y],[w-border_x, h-border_y],[border_x, h-border_y]])
    pst2 = np.float32(list2)#[[x1, y1], [x2, y2], [x3, y3],[x4,y4]])
    M = cv2.getPerspectiveTransform(pst1,pst2)
    dst = cv2.warpPerspective(img, M, (w, h))
    print("perspective")
    # plt.subplot(121), plt.imshow(img), plt.title('Input')
    # plt.subplot(122), plt.imshow(dst), plt.title('Output')
    # plt.show()
    cv2.imwrite(dis_path, dst)
    return

def light(path,path_dis,i):
    img=cv2.imread(path)
    print(path)
    rows,cols,channels=img.shape
    for q in range(5,101,5):
        res=np.uint8(np.clip((1.5*img+q+0.0),0,255))
        # tmp = np.hstack((img, res))  # 两张图片横向合并（便于对比显示）
        # cv2.imshow('image', tmp)
        # cv2.waitKey(0)
        cv2.imwrite(path_dis+"img_"+str(i*1000+100+q)+".jpg",res)
    return

def black(path,path_dis,i):
    img=cv2.imread(path)
    for q in range(5,101,5):
        res=np.uint8(np.clip((1.5*img-q+0.0),0,255))
        # tmp = np.hstack((img, res))  # 两张图片横向合并（便于对比显示）
        # cv2.imshow('image', tmp)
        # cv2.waitKey(0)
        cv2.imwrite(path_dis+"img_"+str(i*1000+200+q)+".jpg",res)
    return
if __name__ == '__main__':
    # readImg("A:/textRe/change_test/img_1.jpg", "A:/textRe/save_test/img_rotate_1.jpg")
    # 补全图片
    count = 500
    border_x = 50
    border_y = 50
    path_res = "A:\\textRe\\change_test\\"
    path_dis = "A:\\textRe\\save_test\\addImage\\"
    path_big = "A:\\textRe\\save_test\\large\\"
    path_light = "A:\\textRe\\save_test\\light\\"
    path_black = "A:\\textRe\\save_test\\black\\"
    path_file = "A:\\textRe\\save_test\\"
    for i in range(1, 501, 1):
        pic_res = "img_" + str(i) + ".jpg"
        count = count + 1
        pic_dis = "img_" + str(500 + i) + ".jpg"  # 加边框 保存地址
        addImage(path_res + pic_res, path_dis + pic_dis, border_x, border_y)
        fo = open(path_file + "per_" + str(i) + ".txt", "w")
        for j in range(1, 101, 1):
            w = 1280
            h = 720
            points = [[0, 0], [0, 0], [0, 0], [0, 0]]  # [[w, h], [w * 2, h], [w * 2, h * 2], [w, 2 * h]]
            list1 = [[border_x, border_y], [w + border_x, border_y], [w + border_x, h + border_y],
                     [border_x, h + border_y]]
            randomPoint(points, "large")
            x1 = points[0][0]
            y1 = points[0][1]
            x2 = points[1][0]
            y2 = points[1][1]
            x3 = points[2][0]
            y3 = points[2][1]
            x4 = points[3][0]
            y4 = points[3][1]
            # affineMap(path_dis + pic_dis, path_big + pic_dis, border_x,border_y,x1, y1, x2, y2, x3, y3, x4, y4)
            count += 1
            perspectiveTransform(path_dis + pic_dis, path_big + "img_" + str(i * 1000 + j) + ".jpg", border_x, border_y,
                                 list1,
                                 points)
            fo.write(
                str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2) + "," + str(x3) + "," + str(y3) + "," + str(
                    x4) + "," + str(y4) + "\n")
            # 关闭打开的文件
        fo.close()
        # for i in range(101,101+20,1):
        light(path_res + pic_res, path_light, i)
        black(path_res + pic_res, path_black, i)
        # watermark(path_res + pic_dis,"","123")
