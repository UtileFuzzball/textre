import random
import cv2
import numpy as np
from shapely.geometry import Polygon,multipolygon,multipoint
from PIL import Image
from matplotlib import pyplot as plt
import numpy as np
def cal_dis(str1,str2):
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)

    c1=poly1.centroid
    c2=poly2.centroid

    # for q in range(array1)
    # poly2.

    print("poly1 poly2 centroid: ",c1,c2,c1.distance(c2))
    pass

def cal_two_iou(str1,str2):
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)

    inter_area = poly1.intersection(poly2).area
    union_area = poly1.area+poly2.area-inter_area
    print("inter_area: "+str(inter_area),end=" ")
    print("a_area: " + str(poly1.area))
    print("b_area: " + str(poly2.area))
    print("union_area: "+str(union_area))
    iou = inter_area/union_area


    print(str(iou))
    print(poly1)
    print(poly2)
    return iou

def cal_iou(imgNum):
    for m in range(1, 501, 1):  # 500张原图
        recall = [0 for i in range(100)]
        precious = [0 for i in range(100)]
        for n in range(1, 101, 1):  # 每张原图100张仿射变换
            flag_b2 = [-1 for i in range(len(b2))]  # 每个测试样本仿射变换比较数组
            # 生成一个 len(b2)*len(b3)大小的数组，比较变换前后对应情况
            for i in range(len(b2)):
                for j in range(len(b3)):
                    iou = cal_iou(b2[i], b3[j])
                    if (iou > 0.5):
                        flag_b2[i] = j
            save(flag_b2)  # 保存比较结果
            recall[n] = search_not_minus1(flag_b2) / len(b2)  # 计算recall
            precious[n] = search_not_minus1(flag_b2) / len(b3)  # 计算precious
def cal_perspectiveM(path_per,path_ori,list_M):
    # with open(path_ori) as file:
    #     for line in file.readlines():
    width=1280
    height=720
    border=50
    ori_array=np.float32([[border,border],[border,width+border],[border+width,border+height],[border,border+height]])
    # list_M=[]
    with open(path_per) as file:
        for line in file.readlines():
            num=line.strip('\n').split(",")
            point_array=np.float32(np.array(num).reshape(-1,2))

            M=cv2.getPerspectiveTransform(ori_array,point_array)
            # M = cv2.getPerspectiveTransform(pst1, pst2)
            # print(point_array)
            # M=cv2.perspectiveTransform(str5,str6)
            list_M.append(M)
            # print(M)
    return M
def get_box_from_res(PATH_RES,box):
    with open(PATH_RES,'r') as file:
        for line in file.readlines():
            tt=np.array(line.strip('\n').split(","),np.float32).reshape(-1,2)
            box.append(tt)
def dot_M(M,box,box2):
    """
    :param M:
    :param box:
    :return: 点乘后的box
    """
    for i in range(len(box)):
        print(box[i])
        pp=[]
        p=np.float32(box[i])
        M=np.float32()
        p_ = cv2.perspectiveTransform(p, M)
        # for j in range(len(box[i])):
            # print("box="+str(type(box[i][j])))
            # box[i][j].np.append(1)
            # p=np.float32(np.append(box[i][j],1))
            # p=(700,98)
            # print(box[i][j])
            # print(p)
            # p=np.float32(np.array(box[i][j].extend([1,])))
            # print("p="+p)
            # print("M="+str(M))
            # print(cv2.warpPerspective(box[i][j],M))
            # p=np.float32(np.array(box[i][j]))

            # p_=np.dot(M,p)
            # print("P_"+str(p_))
            # p_[0]=p_[0]/p_[2]
            # p_[1] = p_[1] / p_[2]
            # p_[2]=p_[2]/p_[2]
        print(str(i)+"=i "+str(j)+"=j "+str(p_))
            # pp.append([p_[0],p_[1]])
        # box2.append(pp)
    # print(box2)
if __name__ == '__main__':
    #读变换后的图片

    str1=[1145, 42, 1144, 29, 1225, 24, 1226, 38]
    str2=[1098, 45, 1097, 32, 1139, 29, 1140, 42]
    str3=[100,150,250,50,400,150,400,250,250,150,100,250]
    str4=[250,150,400,150,550,250,400,250]
    str5=[[100,100],[200,100],[200,200],[100,200]]
    str6=[[100,100],[400,100],[200,200],[100,200]]
    # str5=[100,100,200,100,200,200,100,200]
    # str6=[100,100,400,100,200,200,100,200]
    # cal_two_iou(str1,str2)
    # cal_dis(str3,str4)
    #读出变换的M
    PATH = "D:\\下载\\per_2.txt"
    PATH_RES="D:\\下载\\res_img_2 (2).txt"
    list_M=[]
    cal_perspectiveM(PATH,PATH_RES,list_M)
    for i in range(len(list_M)):
        M=list_M[i]
        box=[]
        box2=[]
        get_box_from_res(PATH_RES,box)
        # print(box)
        # print(M)
        M=np.array(M,dtype=np.float32)
        box=np.array(box,dtype=np.float32)
        print("M=")
        print(M)
        print("box=")
        print(box)
        q=cv2.perspectiveTransform(box[None,:,:],M)
        print(q)


        pass
        # dot_M(M, box,box2)
        # print(str(box))