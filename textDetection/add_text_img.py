import random

import cv2
from shapely.geometry import Polygon
from PIL import Image
from matplotlib import pyplot as plt
def read_test_file(path_test_file,res_pt):
    with open(path_test_file, 'r') as file_to_read:
        for line in file_to_read.readlines():
            # 针对mask
            # pt4_tmp, pt6_temp, pt_v = [str(q) for q in line.split(';')]
            # pt6 = [int(p) for p in pt6_temp.split(',')]
            # res_pt.append(pt6)
            # 针对PSE
            pt = [int(p) for p in line.split(',')]
            res_pt.append(pt)
            # print(line)

def paint_text(pathRes, res_pt, textPoints, allPoints, save_img, list_Text, paintText, colorc):
    """

    :param pathRes:
    :param res_pt: 存储原图包围框的数组信息
    :param textPoints:
    :param allPoints:
    :param save_img:
    :param list_Text: 保存所有文字的位置
    :param paintText:
    :param colorc:
    :return:
    """
    StrText="Q"
    img=cv2.imread(pathRes)
    fontFace=cv2.FONT_HERSHEY_SIMPLEX
    fontScale=colorc[3]
    fontThick=2
    baseLine=0
    ((text_size_width,text_size_height),baseLine)=cv2.getTextSize(paintText,fontFace,fontScale,fontThick)
    textPoints[2]=text_size_width
    textPoints[3]=text_size_height
    for i in range(len(res_pt)):
        ppp = []
        for j in range(0,len(res_pt[i]),2):
            qq=[res_pt[i][j],res_pt[i][j+1]]
            qq_tuple=tuple(qq)
            ppp.append(qq_tuple)
            # print(ppp)
        allPoints.append(ppp)
        # print(allPoints)
    while(True):
        textPoints[0] = random.randint(0, 1280-50)
        textPoints[1] = random.randint(50, 720)
        text_p0=(textPoints[0],textPoints[1]-text_size_height)
        text_p1=(textPoints[0]+text_size_width,textPoints[1]-text_size_height)
        text_p2=(textPoints[0]+text_size_width,textPoints[1]+baseLine)
        text_p3=(textPoints[0],textPoints[1]+baseLine)#左下角加基线
        text_p=[text_p0,text_p1,text_p2,text_p3]
        if produce_text(allPoints,text_p)==True:
            # print("produce_text(allPoints,text_p)==True")
            break
        else:
            textPoints[0] = random.randint(0, 1280-50)
            textPoints[1] = random.randint(45, 720)
            text_p3 = (textPoints[0], textPoints[1])
            # print("produce_text(allPoints,text_p)==false "+textPoints[0]+" "+textPoints[1])
            continue
    ttt=(textPoints[0],textPoints[1])
    # print("ttt"+str(ttt))
    ptext=Polygon(text_p)
    # cv2.putText(img,)
    imgzi=cv2.putText(img,paintText,ttt,cv2.FONT_HERSHEY_SIMPLEX,fontScale,(colorc[0],colorc[1],colorc[2]),2,8)
    list_Text.append(text_p)
    # plt.imshow(imgzi)
    cv2.imwrite(save_img, imgzi)
    # plt.show()
    pass
    pass

def produce_text(allPoints,text_p):
    polygon1=[]
    tmp_polygon=Polygon(text_p) 
    for i in range(len(allPoints)):
        p=Polygon(allPoints[i])
        polygon1.append(p)
    for i in range(len(polygon1)):
        kk=polygon1[i].intersection(tmp_polygon).area
        if(kk>0):
            return False
        else:
            continue
    allPoints.append(text_p)
    return True
def randomText(list_paint_text):
    alphabeta=['z', 'y', 'x', 'w', 'v', 'u', 't', 's', 'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k', 'j', 'i', 'h', 'g', 'f',
               'e', 'd', 'c', 'b', 'a',
               '1','2','3','4','5','6','7','8','9','0',
               'Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q', 'P', 'O', 'N', 'M', 'L', 'K', 'J', 'I', 'H', 'G', 'F',
               'E', 'D', 'C', 'B', 'A']
    # ,'!','@','#','$','%','&','*','^','(',')','-','+','{','}',';','"',
    #           ':',',','.','?','/','\\','~','`','[',']','=']
    for q in range(20):
        num=random.randint(1,15)
        random_str="".join(str(i) for i in random.sample(alphabeta,num))
        list_paint_text.append(random_str)
def randomColor(list_paint_color):
    for q in range(20):
        red=random.randint(1,256)
        green = random.randint(1, 256)
        blue = random.randint(1, 256)
        scale=random.randint(10,21)*1.0/10
        co=[red,green,blue,scale]
        list_paint_color.append(co)
def add_text_img():
    for i in range(1,3,1):
        path_test_file = "A:\\textRe\\submit_ic15 (2)\\res_img_"+str(i)+".txt"
        path_img="A:\\textRe\\icdar2015\\test_images\\img_"+str(i)+".jpg"
        path_save_text="A:\\textRe\\save_test\\watermark\\text\\img_"+str(i)+".txt"
        path_save_color="A:\\textRe\\save_test\\text_color\\img_"+str(i)+".txt"
        """
        存储所有原图的数组
        """
        res_pt1=[]
        read_test_file(path_test_file,res_pt1)
        # print(res_pt1)
        x=100
        y=100
        w=50
        h=100
        pos=[x,y,w,h]
        allpoints=[]
        paintText=[]
        list_paint_Text=[]
        list_text_color=[]
        print(str(i)+"...")
        randomText(list_paint_Text)
        randomColor(list_text_color)
        for j in range(1,21,1):
            save_img = "A:/textRe/save_test/watermark_randrom_"+str(int(i/10))+"/img_" + str(i*1000+300+j) + ".jpg"
            print(save_img)
            paint_text(path_img,res_pt1,pos,allpoints,save_img,paintText,list_paint_Text[j-1],list_text_color[j-1])
            print("    "+str(i)+"  "+str(j))
        with open(path_save_text,'w')as file:
            for q in range(len(paintText)):
                str_list=[]
                for p in range(len(paintText[q])):
                    (a,b)=paintText[q][p]
                    str_list.append(a)
                    str_list.append(b)
                file.write(str(str_list[0]))
                for p in range(1,len(str_list)):
                    file.write(","+str(str_list[p]))
                file.write("\n")
        with open(path_save_color,'w')as file:
            for q in range(len(list_text_color)):
                file.write(list_paint_Text[q]+" ")
                file.write(str(list_text_color[q][0]) + " ")
                file.write(str(list_text_color[q][1]) + " ")
                file.write(str(list_text_color[q][2])+" ")
                file.write(str(list_text_color[q][3]) + " ")
                file.write("\n")


if __name__ == '__main__':
    add_text_img()