import random
import cv2
import numpy as np
from shapely.geometry import Polygon,multipolygon,multipoint
from PIL import Image
from matplotlib import pyplot as plt
import numpy as np

PATH = "D:/下载/per_2.txt"
PATH_RES = "D:/下载/res_img_2 (2).txt"
list_M = []
width = 1280
height = 720
border = 50
ori_array = np.array( [[border, border], [border+width, border], [border + width, border + height], [border, border + height]],dtype=np.float32)
# per_array=np.array([(10,6),(1377,15),(1344,783),(14,810)])
# ori_array=[(50,50),(),(),()]
per_array=[[10,6],[1377,15],[1344,783],[14,810]]
per_array=np.array(per_array,dtype=np.float32)
print(str(per_array))
M=cv2.getPerspectiveTransform(ori_array,per_array)
print(M)
res_box=[]

with open(PATH_RES,'r')as file:
    for line in file.readlines():
        line=file.readline()
        list_temp=[int(i) for i in line.strip('\n').split(',')]
        print(list_temp)
        res_box=np.array(list_temp,dtype=np.float32).reshape(-1,2)
        print(res_box)
        # pp=np.array([],dtype='float32')
        res_box=np.array([res_box])
        per_box=cv2.perspectiveTransform(res_box,M)
        # # cv2.perspectiveTransform()
        print(per_box)
        im=np.zeros((820, 1380,3), dtype=np.uint8)
        ori_array=np.array(ori_array,np.int32).reshape(-1,1,2)
        per_array=np.array(per_array,np.int32).reshape(-1,1,2)
        res_box=np.array(res_box,np.int32).reshape(-1,1,2)
        per_box=np.array(per_box,np.int32).reshape(-1,1,2)
        # plt.imshow(im)
        # plt.show()
        cv2.polylines(im,[ori_array],1,(0,0,255),3)
        cv2.polylines(im,[per_array],1,(0,0,255),3)
        cv2.polylines(im,[per_box],1,(0,0,255),3)
        cv2.polylines(im,[res_box],1,(0,0,255),3)
plt.imshow(im)
plt.show()
# .waitKey(0)
