import matplotlib.image as img
import tensorflow as tf
import numpy as np
sess = tf.InteractiveSession()

#image_train 是训练数据
image_train  = np.zeros((800,112,92))

for i in range(800):
  #  path = ' H:\Python\train_sample\'
    m = str(i+1)
    filename =  "face" + m + '.bmp'
    with tf.Session() as sess:
        image_train [i] = img.imread(filename)

#image_test 测试数据
image_test=np.zeros((1031,112,92))

for i in range(1031):
    m = str(i+1)
    filename = r"H:\Python\test_sample\face" + m + '.bmp'
    with tf.Session() as sess:
        image_test[i] = img.imread(filename)

#test_label
label_test=np.ones((1031,2))
for i in range(591):
    label_test[i,0] = 0
    label_test[i,1] = 1
for i in range(591,1031):
    label_test[i,0] = 1
    label_test[i,1] = 0

#train label
label_train = np.ones((800,2))
for i in range(400):
    label_train[i,0] = 0
    label_train[i,1] = 1
for i in range(400,800):
    label_train[i,0] = 1
    label_train[i,1] = 0

xs = tf.placeholder(tf.float32, shape = [None, 112*92])
ys = tf.placeholder(tf.float32, shape = [None, 2])
keep_prob = tf.placeholder(tf.float32)
x_images=tf.reshape(xs,[-1,112,92,1])

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev = 0.1)
    return tf.Variable(initial)

#get bias
def biases_variable(shape):
    initial = tf.constant(0.1, shape = shape)
    return tf.Variable(initial)

#convolutional layer
def conv2d(x,w):
    return tf.nn.conv2d(x, w, strides = [1, 1, 1, 1], padding = 'SAME')

#pooling layer ##pooling层模版大小为2x2， 所以输出的长宽会变为输入的一半大小
def max_pool(x):
    return tf.nn.max_pool(x, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding = 'SAME')


