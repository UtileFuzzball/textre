import cv2
import numpy as np
from shapely.geometry import Polygon,multipolygon,multipoint
import os
from matplotlib import pyplot as plt
def cal_two_iou(str1,str2):
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)

    inter_area = poly1.intersection(poly2).area
    union_area = poly1.area+poly2.area-inter_area
    iou = inter_area/union_area
    return iou
def cal_box_iou(list_per_box,list_det_box,save_name,index):
    border=50
    width=1280
    height=720

    list_per_flag=[0 for i in range(len(list_per_box))]
    for i in range(len(list_per_box)):
        for j in range(len(list_det_box)):
            if cal_two_iou(list_per_box[i],list_det_box[j])>0.5:
                list_per_flag[i]=j+1
    sum=0
    for i in range(len(list_per_box)):
        if list_per_flag[i]!=0:
            sum=sum+1
        if list_per_flag[i]==0:
            ori_array = np.array(list_per_box[i], np.int32).reshape(-1, 1, 2)
            print(ori_array)

    list_result=[]
    list_result.append(len(list_per_box))#[0]
    list_result.append(len(list_det_box))#[1]
    list_result.append(sum)#[2]
    if(len(list_per_box)==0):
        list_result.append(-1)
    else:
        list_result.append(sum * 1.0 / len(list_per_box))  # [3]
    if(len(list_det_box)==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/len(list_det_box))#[4]
    if((len(list_per_box)+len(list_det_box))==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/(len(list_per_box)+len(list_det_box)))#[5]
    if((len(list_per_box)+len(list_det_box)-sum)==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/(len(list_per_box)+len(list_det_box)-sum))#[6]
    with open(save_name+str(index)+".txt",'a')as file:
        file.write(str(index)+" ")
        file.write(str(list_result[0]) + " ")
        file.write(str(list_result[1]) + " ")
        file.write(str(list_result[2]) + " ")
        file.write(str(list_result[3]) + " ")
        file.write(str(list_result[4]) + " ")
        file.write(str(list_result[5]) + " ")
        file.write(str(list_result[6]) + "\n")
    return list_result

def get_detect_box(path,list_box):
    with open(path,'r')as file:
        for line in file.readlines():
            if line=="\n":
                continue
            num=line.strip('\n').split(',')
            # array_text_box=np.array(num,np.float32).reshape(-1,2)
            for i in range(len(num)):
                num[i]=int(num[i])
            list_box.append(num)
    pass

def compareRGB():
    PATH_DET="A:\\textRe\\0924\\changeRGB-20190924T014340Z-001\\changeRGB\\img_"
    PATH_FILE="A:\\textRe\\0924\\changeRGB_east.txt"
    PATH_FILE_SINGLE="A:\\textRe\\0924\\east_change_rgb_singlefile\\result_"
    result=[]
    # result_path="A:\\textRe\\0924\\changeRGB_psenet.txt"
    for i in range(1,501,1):
        bgr_path="A:\\textRe\\0901\ori-20190901T133949Z-001\\ori\\img_"+str(i)+".txt"
        grb_path=PATH_DET+str(i*1000+601)+".txt"
        rbg_path=PATH_DET+str(i*1000+602)+".txt"
        bgr_List=[]
        grb_List=[]
        rbg_List=[]
        if os.path.exists(bgr_path):
            get_detect_box(bgr_path,bgr_List)
        if os.path.exists(rbg_path):
            get_detect_box(rbg_path, rbg_List)
        if os.path.exists(grb_path):
            get_detect_box(grb_path, grb_List)
        result1=cal_box_iou(bgr_List,grb_List,PATH_FILE_SINGLE,i*1000+601)
        result2= cal_box_iou(bgr_List, rbg_List,PATH_FILE_SINGLE,i*1000+602)
        tt=[]
        tt.append((result1[5]+result2[5])*1.0/2)
        tt.append((result1[6] + result2[6]) * 1.0 / 2)
        with open(PATH_FILE,'a')as f:
            f.write(str(i)+" ")
            f.write(str(tt[0])+" ")
            f.write(str(tt[1])+"\n")

if __name__ == '__main__':
    compareRGB()