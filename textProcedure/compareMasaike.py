import os
def get_detect_box(path,list_box):
    with open(path,'r')as file:
        for line in file.readlines():
            if line=="\n":
                continue
            num=line.strip('\n').split(',')
            # array_text_box=np.array(num,np.float32).reshape(-1,2)
            for i in range(len(num)):
                num[i]=int(num[i])
            list_box.append(num)
    pass
def compareMasaike():
    PATH_DET="A:\\textRe\\0924\\masaike-20190923T022406Z-001\\masaike\\img_"
    PATH_FILE="A:\\textRe\\0924\\masaike_east.txt"
    num_list=[]
    for i in range(1,501,1):
        path_det=PATH_DET+str(i*1000+700)+".txt"
        list_det=[]
        if os.path.exists(path_det):
            get_detect_box(path_det,list_det)
        num_list.append(len(list_det))
    with open(PATH_FILE,'a') as file:
        for i in range(len(num_list)):
            file.write(str(i+1)+" "+str(num_list[i])+"\n")
    pass

def compareRGB():
    PATH_DET="A:\\textRe\\0924\\masaike-20190923T022406Z-001\\masaike\\img_"
    PATH_FILE="A:\\textRe\\0924\\changeRGB_psenet.txt"
    for i in range(1,501,1):
        bgr_path=""
        grb_path=""
        rbg_path=""
        bgr_List=[]
        grb_List=[]
        rbg_List=[]
        if os.path.exists(bgr_path):
            get_detect_box(bgr_path,bgr_List)
        if os.path.exists(rbg_path):
            get_detect_box(rbg_path, rbg_List)
        if os.path.exists(grb_path):
            get_detect_box(grb_path, grb_List)

if __name__ == '__main__':
    # compareMasaike()
    pass