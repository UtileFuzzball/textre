#!user/bin/python3
from PIL import Image
import cv2
import numpy as np
from matplotlib import pyplot as plt
import random


def readImg(path, dis_path):
    im = Image.open(path, "r")  # "A:/textRe/change_test/img_"+str(i)+".jpg","r")
    # im.show()
    resizeImg(path, dis_path)

def resizeImg(path, dst_path):
    # print(path)
    # im.show()
    img = cv2.imread(path, 1)
    img_info = img.size
    height = img_info[0]
    width = img_info[1]
    mode = img_info[2]
    dst_height = int(height * 2)
    dst_width = int(width * 0.5)

    dst = cv2.resize(img, (dst_width, dst_height))
    cv2.imshow('image', dst)
    cv2.waitKey(3000)
    cv2.imwrite(dst_path, dst)

def changeRGB(path_res,dst_path,index):
    src = cv2.imread(path_res)
    b,g,r=cv2.split(src)
    bgr=cv2.merge([b,g,r])
    cv2.imshow('bgr', bgr)
    grb=cv2.merge([g,r,b])
    cv2.imshow('grb', grb)
    rbg=cv2.merge([r,b,g])
    cv2.imshow('rbg', rbg)

    cv2.waitKey(0)
    str_grb=str(index*1000+600+1)+".jpg"
    str_rbg = str(index * 1000 + 600 + 2) + ".jpg"
    cv2.imwrite(dst_path+str_grb, grb)
    cv2.imwrite(dst_path+str_rbg, rbg)


    # print(src.shape)

if __name__ == '__main__':
    # 补全图片
    count = 500
    path_res = "A:\\textRe\\change_test\\"
    path_dis = "A:\\textRe\\save_test\\addImage\\"
    path_big = "A:\\textRe\\save_test\\large\\"
    path_light = "A:\\textRe\\save_test\\light\\"
    path_black = "A:\\textRe\\save_test\\black\\"
    path_file = "A:\\textRe\\save_test\\"
    pic_dis_rbg="A:\\textRe\\save_test\\changeRGB\\"
    for i in range(1, 5, 1):
        pic_res = path_res+"img_" + str(i) + ".jpg"

        # count = count + 1
        pic_dis_rgb = "img_" + str(i*1000+5000) #+ ".jpg"  # 加边框 保存地址6001 6002 6003 6004 6005
        changeRGB(pic_res, pic_dis_rgb,i)
        # addImage(path_res + pic_res, path_dis + pic_dis, border_x, border_y)
        # fo = open(path_file + "per_" + str(i) + ".txt", "w")

