#!user/bin/python3
from PIL import Image
import cv2
import numpy as np
from matplotlib import pyplot as plt
import random


def readImg(path, dis_path):
    im = Image.open(path, "r")  # "A:/textRe/change_test/img_"+str(i)+".jpg","r")
    # im.show()
    resizeImg(path, dis_path)


def affineMap(path, dis_path, border_x,border_y, x1, y1, x2, y2, x3, y3, x4, y4):
    img = cv2.imread(path)
    img_info = img.shape
    w = img_info[1]
    h = img_info[0]
    channel = img_info[2]
    print("rows" + str(w) + "cols" + str(h), "channel" + str(channel))
    pst1 = np.float32([[border_x, border_y], [border_x*2, border_y],
                       [border_x*2, border_y*2]])  # [w-border/2, h-border/2]
    pst2 = np.float32([[x1, y1], [x2, y2], [x3, y3]])
    M = cv2.getAffineTransform(pst1, pst2)
    dst = cv2.warpAffine(img, M, (w, h))
    plt.subplot(121), plt.imshow(img), plt.title('Input')
    plt.subplot(122), plt.imshow(dst), plt.title('Output')
    plt.show()
    cv2.imwrite(dis_path, dst)

def perspectiveTransform(path, dis_path, border_x,border_y, list1,list2):
    img = cv2.imread(path)
    img_info = img.shape
    w = img_info[1]
    h = img_info[0]
    #border_x=50
    #border_y=50
    channel = img_info[2]
    print("rows" + str(w) + "cols" + str(h), "channel" + str(channel))
    pst1 = np.float32(list1)#[[border_x, border_y], [w-border_x, border_y],[w-border_x, h-border_y],[border_x, h-border_y]])
    pst2 = np.float32(list2)#[[x1, y1], [x2, y2], [x3, y3],[x4,y4]])
    M = cv2.getPerspectiveTransform(pst1,pst2)
    dst = cv2.warpPerspective(img, M, (w, h))
    print("perspective")
    # plt.subplot(121), plt.imshow(img), plt.title('Input')
    # plt.subplot(122), plt.imshow(dst), plt.title('Output')
    # plt.show()
    cv2.imwrite(dis_path, dst)
    return

def resizeImg(path, dst_path):
    # print(path)
    # im.show()
    img = cv2.imread(path, 1)
    img_info = img.size
    height = img_info[0]
    width = img_info[1]
    mode = img_info[2]
    dst_height = int(height * 2)
    dst_width = int(width * 0.5)

    dst = cv2.resize(img, (dst_width, dst_height))
    cv2.imshow('image', dst)
    cv2.waitKey(3000)
    cv2.imwrite(dst_path, dst)


def image_in_image(img, background, fx):
    fx = background.shape[0] * fx / img.shape[0]
    fx = min(fx, background.shape[1] * fx / img.shape[1])
    fx = min(fx, background.shape[1] / img.shape[1] - 0.01, background.shape[0] / img.shape[0] - 0.01)
    fx = max(fx, 0.08)

    dst = cv2.resize(img, (0, 0), fx=fx, fy=fx, interpolation=cv2.INTER_AREA)

    dst_w, dst_h = dst.shape[:2]

    left = random.randint(0, background.shape[0] - dst_w - 1)
    top = random.randint(0, background.shape[1] - dst_h - 1)
    # cv2.addWeighted(src1, alpha, src2, beta, gamma[, dst[, dtype]])
    # dst = src1 * alpha + src2 * beta + gamma;
    background[left:left + dst_w, top:top + dst_h] = cv2.addWeighted(
        background[left:left + dst_w, top:top + dst_h], 0, dst, 1, 0
    )

    return background


def addImage(path, path_dist, border_x, border_y):
    png = path
    img = Image.open(png)
    info = img.size
    w = info[0]
    h = info[1]
    # print(str(w)+"w是1维"+str(h))
    new_pic = Image.new('RGB', (w + border_x * 2, h + border_y * 2), color="black")
    new_pic.paste(img, (int(border_x), int(border_y)))
    new_pic.save(path_dist)
    print("addimage"+str(new_pic.size))

def readfile(filename):
    pos = []
    Efield = []
    with open(filename, 'r') as file_to_read:
        while True:
            lines = file_to_read.readline()  # 整行读取数据
            if not lines:
                break
                pass
                p_tmp, E_tmp = [float(i) for i in lines.split()]  # 将整行数据分割处理，如果分割符是空格，括号里就不用传入参数，如果是逗号， 则传入‘，'字符。
                pos.append(p_tmp)  # 添加新读取的数据
                Efield.append(E_tmp)
                pass
        pos = np.array(pos)  # 将数据从list类型转换为array类型。
        Efield = np.array(Efield)
    pass
def read_test_file(path_test_file):
    with open(path_test_file,'r') as file_to_read:
        for line in file_to_read.readlines():
            pt4_tmp,pt6_temp,pt_v=[str(i) for i in line.split(';')]
            pt6=[int(i) for i in pt6_temp.split(',')]


def light(path,path_dis,i):
    img=cv2.imread(path)
    print(path)
    rows,cols,channels=img.shape
    for q in range(5,101,5):
        res=np.uint8(np.clip((1.5*img+q+0.0),0,255))
        # tmp = np.hstack((img, res))  # 两张图片横向合并（便于对比显示）
        # cv2.imshow('image', tmp)
        cv2.waitKey(0)
        cv2.imwrite(path_dis+"img_"+str(i*1000+100+q)+".jpg",res)
    return

def black(path,path_dis,i):
    img=cv2.imread(path)
    for q in range(5,101,5):
        res=np.uint8(np.clip((1.5*img-q+0.0),0,255))
        tmp = np.hstack((img, res))  # 两张图片横向合并（便于对比显示）
        # cv2.imshow('image', tmp)
        cv2.waitKey(0)
        cv2.imwrite(path_dis+"img_"+str(i*1000+200+q)+".jpg",res)
    return

# 单独 拉伸x轴y轴试验
# 补充 缩小
# 水印直接 加文字 先不加透明度
def randomPoint(points,change_type):
    #放大
    #border_x=50
    #border_y=50
    print("global variables")
    # print(globals())
    if change_type=="large":
        points[0][0] = random.randint(0,border_x)
        points[0][1] = random.randint(0,border_y)
        points[1][0] = random.randint(1280+border_x, 1280+2*border_x)
        points[1][1] = random.randint(0, border_y)

        points[2][0] = random.randint(1280+border_x, 1280+2*border_x)
        points[2][1] = random.randint(720+border_y, 720+border_y*2)
        points[3][0] = random.randint(0, border_x)
        points[3][1] = random.randint(720+border_y,720+2*border_y)
    elif change_type=="small":
        points[0][0] = random.randint(0,border_x)
        points[0][1] = random.randint(0,border_y)
        points[1][0] = random.randint(1280-border_x, 1280)
        points[1][1] = random.randint(0, border_y)

        points[2][0] = random.randint(1280-border_x, 1280)
        points[2][1] = random.randint(720-border_y, 720)
        points[3][0] = random.randint(0, border_x)
        points[3][1] = random.randint(720-border_y,720)

    print(change_type+" random Points: ",)
    print(points)
    # 缩小
    # y轴拉伸
    # x轴拉伸

def watermark(path,path_dis,markStr,path_mask,transparency):
    mask=cv2.imread(path_mask)
    m_rows, m_cols, m_channels = mask.shape
    img=cv2.imread(path)
    w=img.shape[1]
    h=img.shape[0]
    print("w=%d h=%d"% (w,h))
    # cv2.putText(img,markStr,)
    mask = np.floor(mask.astype(np.float32) * transparency).astype(np.uint8)
    dst = cv2.add(img, mask)
    # 防止溢出像素值域
    dst = dst * (dst <= 255) + 255 * (dst > 255)
    # 回到uint8型
    dst = dst.astype(np.uint8)
    # return dst

    b_rows, b_cols, b_channels = mask.shape
    m_rows, m_cols, m_channels = mask.shape
    # 将图片右下角矩形块 替换为 加过水印的矩形块
    img[b_rows - m_rows:b_rows, b_cols - m_cols:b_cols, ] = mask



if __name__ == '__main__':
    # 补全图片
    count=500
    border_x = 50
    border_y = 50
    path_res = "A:\\textRe\\change_test\\"
    path_dis = "A:\\textRe\\save_test\\addImage\\"
    path_big = "A:\\textRe\\save_test\\large\\"
    path_light="A:\\textRe\\save_test\\light\\"
    path_black = "A:\\textRe\\save_test\\black\\"
    path_file="A:\\textRe\\save_test\\"
    for i in range(1,501,1):
        pic_res="img_"+str(i)+".jpg"
        count=count+1
        pic_dis="img_"+str(500+i)+".jpg"#加边框 保存地址
        addImage(path_res+pic_res, path_dis+pic_dis,border_x,border_y)
        fo = open(path_file + "per_" + str(i) + ".txt", "w")
        for j in range (1,101,1):
            w = 1280
            h = 720
            points =[[0,0],[0,0],[0,0],[0,0]] # [[w, h], [w * 2, h], [w * 2, h * 2], [w, 2 * h]]
            list1 = [[border_x, border_y], [w+border_x, border_y],[w+border_x, h+border_y],[border_x, h+border_y]]
            randomPoint(points,"large")
            x1=points[0][0]
            y1=points[0][1]
            x2=points[1][0]
            y2=points[1][1]
            x3=points[2][0]
            y3=points[2][1]
            x4=points[3][0]
            y4=points[3][1]
            #affineMap(path_dis + pic_dis, path_big + pic_dis, border_x,border_y,x1, y1, x2, y2, x3, y3, x4, y4)
            count+=1
            perspectiveTransform(path_dis + pic_dis, path_big + "img_"+str(i*1000+j)+".jpg", border_x,border_y,list1,
                                 points)
            fo.write(str(x1)+","+str(y1)+","+str(x2)+","+str(y2)+","+str(x3)+","+str(y3)+","+str(x4)+","+str(y4)+"\n")
            # 关闭打开的文件
        fo.close()
        # for i in range(101,101+20,1):
        light(path_res + pic_res,path_light,i)
        black(path_res + pic_res, path_black,i)
        # watermark(path_res + pic_dis,"","123")