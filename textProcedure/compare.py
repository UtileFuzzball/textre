import random
import cv2
from shapely.geometry import Polygon,multipolygon,multipoint
from PIL import Image
from matplotlib import pyplot as plt
import numpy as np
def cal_dis(str1,str2):
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)

    c1=poly1.centroid
    c2=poly2.centroid

    # for q in range(array1)
    # poly2.

    print("poly1 poly2 centroid: ",c1,c2,c1.distance(c2))
    pass

def cal_iou(str1,str2):
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)

    inter_area = poly1.intersection(poly2).area
    union_area = poly1.area+poly2.area-inter_area
    print("inter_area: "+str(inter_area),end=" ")
    print("a_area: " + str(poly1.area))
    print("b_area: " + str(poly2.area))
    print("union_area: "+str(union_area))
    iou = inter_area/union_area


    print(str(iou))
    print(poly1)
    print(poly2)
    return iou

if __name__ == '__main__':

    str1=[1145, 42, 1144, 29, 1225, 24, 1226, 38]
    str2=[1098, 45, 1097, 32, 1139, 29, 1140, 42]
    str3=[100,150,250,50,400,150,400,250,250,150,100,250]
    str4=[250,150,400,150,550,250,400,250]
    cal_iou(str1,str2)
    cal_dis(str3,str4)