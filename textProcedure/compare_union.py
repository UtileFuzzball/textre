import os
import random
from typing import List, Any, Union

import cv2
from shapely.geometry import Polygon,multipolygon,multipoint
from PIL import Image
from matplotlib import pyplot as plt
import numpy as np

def cal_three_iou(str1,str2,str3):
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)

    array3 = np.array(str3).reshape(-1,2)
    poly3 = Polygon(array3)

    inter_area1 = poly1.intersection(poly2)
    inter_area2 = poly1.intersection(poly3)
    inter_area3 = poly2.intersection(poly3)
    inter_area=inter_area1.intersection(poly3)
    union_area = poly1.area+poly2.area+poly3.area+inter_area.area-inter_area1.area-inter_area2.area-inter_area3.area
    # print("inter_area: "+str(inter_area),end=" ")
    # print("a_area: " + str(poly1.area))
    # print("b_area: " + str(poly2.area))
    # print("union_area: "+str(union_area))
    iou = inter_area.area/union_area
    return iou
def cal_two_iou(str1,str2):
    # print("str1="+str(str1))
    # print("str2="+str(str2))
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)

    inter_area = poly1.intersection(poly2).area
    union_area = poly1.area+poly2.area-inter_area
    # print("inter_area: "+str(inter_area),end=" ")
    # print("a_area: " + str(poly1.area))
    # print("b_area: " + str(poly2.area))
    # print("union_area: "+str(union_area))
    iou = inter_area/union_area
    # print(str(iou))
    # print(poly1)
    # print(poly2)
    return iou
def cal_iou(file_num,list_pse,list_pix,list_east):
    pass
    print("this is cal iou")
    flag_pse_pix=[0 for i in range(len(list_pse))]
    for b1 in range(len(list_pse)):
        for b2 in range(len(list_pix)):
            if cal_two_iou(list_pse[b1],list_pix[b2])>0.5:
                flag_pse_pix[b1]= b2+1
    print("pse pix")
    print(flag_pse_pix)
    flag_pse_east=[0 for i in range(len(list_pse))]
    for b1 in range(len(list_pse)):
        for b3 in range(len(list_east)):
            if cal_two_iou(list_pse[b1],list_east[b3])>0.5:
                flag_pse_east[b1]= b3+1
    print("pse east")
    print(flag_pse_east)
    flag_pix_east=[0 for i in range(len(list_pix))]
    for b2 in range(len(list_pix)):
        for b3 in range(len(list_east)):
            if cal_two_iou(list_pix[b2],list_east[b3])>0.5:
                flag_pix_east[b2]= b3+1
    print("pix east")
    print(flag_pix_east)
    flag_all=[[0] * len(list_pix) for i in range(len(list_pse))]
    for b1 in range(len(list_pse)):
        for b2 in range(len(list_pix)):
            for b3 in range(len(list_east)):
                if cal_three_iou(list_pse[b1],list_pix[b2],list_east[b3])>0.5:
                    flag_all[b1][b2]=b3+1
    sum1 = 0
    sum2 = 0
    sum3 = 0
    sum  = 0
    for i in range(len(list_pse)):
        if flag_pse_pix[i] != 0:
            sum1 = sum1 + 1
    for i in range(len(list_pse)):
        if flag_pse_east[i] != 0:
            sum2 = sum2 + 1
    for i in range(len(list_pix)):
        if flag_pix_east[i] != 0:
            sum3 = sum3 + 1
    for i in range(len(list_pse)):
        for j in range(len(list_pix)):
            if flag_all[i][j]!=0:
                sum=sum+1
    list_result=[]
    list_result.append(file_num)
    list_result.append(len(list_pse))
    list_result.append(len(list_pix))
    list_result.append(len(list_east))
    list_result.append(sum1)
    list_result.append(sum2)
    list_result.append(sum3)
    list_result.append(sum)
    if len(list_pse)!=0 and len(list_pix)!=0:
        list_result.append(sum1*1.0/(len(list_pse)+len(list_pix)-sum1))
    else:
        list_result.append(-1)
    if len(list_pse) != 0 and len(list_east) != 0:
        list_result.append(sum2 * 1.0 / (len(list_pse) + len(list_east) - sum2))
    else:
        list_result.append(-1)

    if len(list_pix) != 0 and len(list_east) != 0:
        list_result.append(sum3 * 1.0 / (len(list_pix) + len(list_east) - sum3))
    else:
        list_result.append(-1)
    if len(list_pix) != 0 and len(list_east) != 0 and len(list_pse) != 0:
        if ((len(list_pix) + len(list_east) + len(list_pse) - sum1 - sum2 - sum3 + sum) != 0):
            list_result.append(sum * 1.0 / (len(list_pix) + len(list_east)+len(list_pse)-sum1 - sum2-sum3+sum))
        if ((len(list_pix) + len(list_east) + len(list_pse) - sum1 - sum2 - sum3 + sum) == 0):
            list_result.append(1)
    else:
        list_result.append(-1)
    return list_result

def read_file(path,res_pt):
    print("read"+path)
    if os.path.exists(path):
        with open(path,'r') as file:
                file_list=file.readlines()
                if file_list:
                    for line in file_list:
                        if line:
                            if line !='\n':
                                p_temp=[int(i) for i in line.split(',')]
                                res_pt.append(p_temp)
                    for line2 in res_pt:
                        print(line2)
#pixlink read file

def read_one_file(file_num,list_ori_pse,list_ori_pix,list_ori_east):
    path_pse = "A:\\textRe\\1125\\psenet\\res_img_"+str(file_num)+".txt"
    path_pix = "A:\\textRe\\1125\\pixlink\\ori_light\\txt\\res_img_"+str(file_num)+".txt"
    path_east= "A:\\textRe\\1125\\east\\ori\\img_"+str(file_num)+".txt"
    # list_ori_pse = []
    # list_ori_pix = []
    # list_ori_east = []
    read_file(path_pse, list_ori_pse)
    read_file(path_pix, list_ori_pix)
    read_file(path_east, list_ori_east)

if __name__ == '__main__':
    result=[]
    path_average="A:\\textRe\\1125\\average.txt"
    for i in range(1,501):
        print("file "+str(i))
        pse_pt=[]
        pix_pt=[]
        east_pt=[]
        read_one_file(i,pse_pt,pix_pt,east_pt)
        print(pse_pt)
        print(pix_pt)
        print(east_pt)
        temp=cal_iou(i,pse_pt,pix_pt,east_pt)
        result.append(temp)
        print(temp)
    with open(path_average, 'w')as file:
        for m in range(len(result)):
            for n in range(len(result[m])):
                file.write(" " +str(result[m][n]))#
            file.write("\n")