import random

import cv2
from shapely.geometry import Polygon
from PIL import Image
from matplotlib import pyplot as plt
def read_test_file(path_test_file,res_pt):
    with open(path_test_file, 'r') as file_to_read:
        for line in file_to_read.readlines():
            # 针对mask
            # pt4_tmp, pt6_temp, pt_v = [str(q) for q in line.split(';')]
            # pt6 = [int(p) for p in pt6_temp.split(',')]
            # res_pt.append(pt6)
            # 针对PSE
            pt = [int(p) for p in line.split(',')]
            res_pt.append(pt)
            print(line)

def paint_text(pathRes,res_pt,textPoints,allPoints,save_img):
    # pt1=(textPoints[0],textPoints[1])
    # pt2=(textPoints[0]+w,textPoints[1]+h)
    # allPoints=[]
    StrText="helloworld"
    img=cv2.imread(pathRes)
    fontFace=cv2.FONT_HERSHEY_SIMPLEX
    fontScale=2
    fontThick=1
    baseLine=0
    # print(cv2.getTextSize(StrText,fontFace,fontScale,fontThick))
    ((text_size_width,text_size_height),baseLine)=cv2.getTextSize(StrText,fontFace,fontScale,fontThick)
    textPoints[2]=text_size_width
    textPoints[3]=text_size_height
    for i in range(len(res_pt)):
        ppp = []
        for j in range(0,len(res_pt[i]),2):
            qq=[res_pt[i][j],res_pt[i][j+1]]
            qq_tuple=tuple(qq)
            # print(qq_tuple)
            # print((res_pt[i]))
            # print(res_pt[i][j],)
            # print(res_pt[i][j+1], )
            ppp.append(qq_tuple)
            # print(ppp)
        allPoints.append(ppp)
        # allPoints.extend(ppp)
        # print(allPoints)
    while(True):
        textPoints[0] = random.randint(0, 1280)
        textPoints[1] = random.randint(0, 720)
        text_p0=(textPoints[0],textPoints[1]-text_size_height)
        text_p1=(textPoints[0]+text_size_width,textPoints[1]-text_size_height)
        text_p2=(textPoints[0]+text_size_width,textPoints[1])
        text_p3=(textPoints[0],textPoints[1])
        text_p=[text_p0,text_p1,text_p2,text_p3]
        if produce_text(allPoints,text_p)==True:
            # print("produce_text(allPoints,text_p)==True")
            break
        else:

            textPoints[0] = random.randint(0, 1280)
            textPoints[1] = random.randint(0, 720)
            text_p3 = (textPoints[0], textPoints[1])
            # print("produce_text(allPoints,text_p)==false "+textPoints[0]+" "+textPoints[1])
            continue
    ttt=(textPoints[0],textPoints[1])
    # print("ttt"+str(ttt))
    ptext=Polygon(text_p)
    imgzi=cv2.putText(img,StrText,ttt,cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),2,8)

    plt.imshow(imgzi)
    cv2.imwrite(save_img, imgzi)
    # plt.show()

def produce_text(allPoints,text_p):
    polygon=[]
    tmp_polygon=Polygon(text_p)
    for i in range(len(allPoints)):
        p=Polygon(allPoints[i])
        # print(p)
        polygon.append(p)
    for i in range (len(polygon)):
        kk=polygon[i].intersection(tmp_polygon).area
        # print(kk.__str__())
        if(kk>0):
            # print()
            return False
        else:
            continue
    allPoints.append(text_p)
    return True

if __name__ == '__main__':
    # path_test_file = "A:\\pycharm\\textProcedure\\res_img_3.txt"
    # res_pt1=[]
    # read_test_file(path_test_file,res_pt1)
    # x=100
    # y=100
    # w=50
    # h=100
    # pos=[x,y,w,h]
    # allpoints=[]
    for i in range(1,501,1):
        path_test_file = "A:\\textRe\\submit_ic15 (2)\\res_img_"+str(i)+".txt"
        path_img="A:\\textRe\\icdar2015\\test_images\\img_"+str(i)+".jpg"
        res_pt1=[]
        read_test_file(path_test_file,res_pt1)
        x=100
        y=100
        w=50
        h=100
        pos=[x,y,w,h]
        allpoints=[]
        print(str(i))
        for j in range(1,11,1):
            save_img = "A:\\textRe\\save_test\\watermark\\img_" + str(i*1000+300+j) + ".jpg"
            paint_text(path_img,res_pt1,pos,allpoints,save_img)
    # rectangle(100,100,100,100,"helloworld")
