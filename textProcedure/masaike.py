import random
import os
import cv2
import numpy as np
from shapely.geometry import Polygon,multipolygon,multipoint
from PIL import Image
from matplotlib import pyplot as plt
import numpy as np
import copy
def cal_two_iou(str1,str2):
    for x in range(0,len(str1),2):
        if str1[x]>=1280:
            str1[x]=1279
    for y in range(1,len(str1),2):
        if str1[y]>=720:
            str1[y]=719
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    for x in range(0,len(str2),2):
        if str2[x]>=1280:
            str2[x]=1279
    for y in range(1,len(str2),2):
        if str2[y]>=720:
            str2[y]=719
    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)
    inter_area = poly1.intersection(poly2).area
    union_area = poly1.area+poly2.area-inter_area
    iou = inter_area/union_area
    return iou

def cal_box_iou(list_per_box,list_det_box,newborder,save_name,newtextbox):
    border=0
    width=1280
    height=720
    im = np.zeros((720, 1280, 3), dtype=np.uint8)
    list_per_flag=[0 for i in range(len(list_det_box))]
    for i in range(len(list_per_box)):
        for j in range(len(list_det_box)):
            if cal_two_iou(list_per_box[i],list_det_box[j])>0.5:
                list_per_flag[j]=i+1
    sum=0
    newTextFlag=0
    for i in range(len(list_det_box)):
        if list_per_flag[i]!=0:
            sum=sum+1
        if list_per_flag[i]==0:
            ori_array = np.array(list_det_box[i], np.int32).reshape(-1, 1, 2)
            cv2.polylines(im, [ori_array], 1, (255, 255, 255), 5)
            print(ori_array)
            if cal_two_iou(list_det_box[i],newtextbox)>0.5:
                newTextFlag=1
    for i in range(len(list_per_box)):
        ori_array=np.array(list_per_box[i],np.int32).reshape(-1,1,2)
        cv2.polylines(im,[ori_array],1,(255,0,0),3)
    for j in range(len(list_det_box)):
        ori_array=np.array(list_det_box[j],np.int32).reshape(-1,1,2)
        cv2.polylines(im,[ori_array],1,(0,255,0),3)
    plt.imsave(save_name,im)

    list_result=[]
    list_result.append(len(list_per_box))#[0]
    list_result.append(len(list_det_box))#[1]
    list_result.append(sum)#[2]
    if(len(list_per_box)==0):
        list_result.append(-1)
    else:
        list_result.append(sum * 1.0 / len(list_per_box))  # [3]
    if(len(list_det_box)==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/len(list_det_box))#[4]
    if((len(list_per_box)+len(list_det_box))==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/(len(list_per_box)+len(list_det_box)))#[5]
    if((len(list_per_box)+len(list_det_box)-sum)==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/(len(list_per_box)+len(list_det_box)-sum))#[6]
    list_result.append(newTextFlag) #[7]
    return list_result

def get_detect_box(path,list_box):
    with open(path,'r')as file:
        for line in file.readlines():
            if line=="\n":
                continue
            num=line.strip('\n').split(',')
            for i in range(len(num)):
                num[i]=int(num[i])
            list_box.append(num)
    pass

def add_masaike():
    without_file=[]
    PATH_WITHOUTFILE="A:\\textRe\\0918\\withoutfile_MSAIKE.txt"
    PATH_PAINT="A:\\textRe\\icdar2015\\test_images\\img_" #原图
    PATH_RES = "A:\\textRe\\0901\\ori-20190901T133949Z-001\\ori\\img_" #识别出的list
    PATH_SAVE_MSK_IMG="A:\\textRe\\0918\\masaike\\img_" #保存加马赛克的图

    for i in range(1,501, 1):
        print("now is the pic........................."+str(i))
        path_res = PATH_RES + str(i) + ".txt"
        path_paint=PATH_PAINT+str(i)+".jpg"
        path_save_img=PATH_SAVE_MSK_IMG+str(i*1000+700)+".jpg"
        img=cv2.imread(path_paint)
        list_box = []
        if os.path.exists(path_res):
            get_detect_box(path_res, list_box)
        else:
            print(str(i)+" not have file")
            without_file.append(i)
        for j in range(len(list_box)):
            array=list_box[j]
            x,y,w,h=findMessage(array)
            img=do_mosaic(img,x,y,w,h)
        cv2.imwrite(path_save_img,img)
    with open(PATH_WITHOUTFILE,'a') as file:
        for k in range(len(without_file)):
            file.write(str(without_file[k])+"\n")

def findMessage(list):
    minX=5000
    minY=5000
    maxW=0
    maxH=0
    print(list)
    array=np.array(list).reshape(-1,2)
    for i in range(len(array)):
        if minX > array[i][0]:
            minX = array[i][0]
        if minY > array[i][1]:
            minY = array[i][1]
    for i in range(len(array)):
        w=array[i][0]-minX
        h=array[i][1]-minY
        if maxW<w:
            maxW=w
        if maxH<h:
            maxH=h
    return [minX,minY,maxW,maxH]

def do_mosaic(frame, x, y, w, h, neighbor=9):
    """
    马赛克的实现原理是把图像上某个像素点一定范围邻域内的所有点用邻域内左上像素点的颜色代替，这样可以模糊细节，但是可以保留大体的轮廓。
    :param frame: opencv frame
    :param int x :  马赛克左顶点
    :param int y:  马赛克右顶点
    :param int w:  马赛克宽
    :param int h:  马赛克高
    :param int neighbor:  马赛克每一块的宽
    """
    fh, fw = frame.shape[0], frame.shape[1]
    frame1=copy.deepcopy(frame)
    if (y + h+neighbor > fh):
        h=fh-y-neighbor
    if (x + w+neighbor> fw):
        w=fw-x-neighbor
    for i in range(0, h+neighbor, neighbor):  # 关键点0 减去neightbour 防止溢出
        for j in range(0, w+neighbor, neighbor):
            rect = [j + x, i + y, neighbor, neighbor]
            color = frame[i + y][j + x].tolist()  # 关键点1 tolist
            print(color)
            left_up = (rect[0], rect[1])
            right_down = (rect[0] + neighbor - 1, rect[1] + neighbor - 1)  # 关键点2 减去一个像素
            cv2.rectangle(frame1, left_up, right_down, color, -1)
    return frame1
if __name__ == '__main__':
    add_masaike()