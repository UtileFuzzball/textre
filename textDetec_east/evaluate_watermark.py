import random
import os
import cv2
import numpy as np
from shapely.geometry import Polygon,multipolygon,multipoint
from PIL import Image
from matplotlib import pyplot as plt
import numpy as np
import copy
def cal_two_iou(str1,str2):
    # print("str1="+str(str1))
    # print("str2="+str(str2))
    for x in range(0,len(str1),2):
        if str1[x]>=1280:
            str1[x]=1279
    for y in range(1,len(str1),2):
        if str1[y]>=720:
            str1[y]=719
    array1 = np.array(str1).reshape(-1,2)
    poly1 = Polygon(array1)

    # for x in range()
    for x in range(0,len(str2),2):
        if str2[x]>=1280:
            str2[x]=1279
    for y in range(1,len(str2),2):
        if str2[y]>=720:
            str2[y]=719
    array2 = np.array(str2).reshape(-1,2)
    poly2 = Polygon(array2)

    inter_area = poly1.intersection(poly2).area
    union_area = poly1.area+poly2.area-inter_area
    # print("inter_area: "+str(inter_area),end=" ")
    # print("a_area: " + str(poly1.area))
    # print("b_area: " + str(poly2.area))
    # print("union_area: "+str(union_area))
    iou = inter_area/union_area
    # print(str(iou))
    # print(poly1)
    # print(poly2)
    return iou

def cal_box_iou(list_per_box,list_det_box,newborder,save_name,newtextbox):
    border=0
    width=1280
    height=720
    im = np.zeros((720, 1280, 3), dtype=np.uint8)
    # cv2.polylines(im, [np.array(newborder,np.int32).reshape(-1,1,2)], 1, (0, 255, 0), 3)
    # cv2.polylines(im,[np.array([[border,border],[border+width,border],[border+width,border+height],[border,border+height]],np.int32).reshape(-1,1,2)],1,(255,0,0),3)
    list_per_flag=[0 for i in range(len(list_det_box))]
    for i in range(len(list_per_box)):
        for j in range(len(list_det_box)):
            # print("list_per_box"+str(i))
            # print(list_per_box[i])
            if cal_two_iou(list_per_box[i],list_det_box[j])>0.5:
                list_per_flag[j]=i+1
                # print("iou >0.5")
    sum=0
    newTextFlag=0
    for i in range(len(list_det_box)):
        if list_per_flag[i]!=0:
            sum=sum+1
        if list_per_flag[i]==0:
            ori_array = np.array(list_det_box[i], np.int32).reshape(-1, 1, 2)
            cv2.polylines(im, [ori_array], 1, (255, 255, 255), 5)
            print(ori_array)
            if cal_two_iou(list_det_box[i],newtextbox)>0.5:
                newTextFlag=1
    # im = np.zeros((820, 1380, 3), dtype=np.uint8)
    for i in range(len(list_per_box)):
        ori_array=np.array(list_per_box[i],np.int32).reshape(-1,1,2)
        # per_array=np.array(per_array,np.int32).reshape(-1,1,2)
        # res_box=np.array(res_box,np.int32).reshape(-1,1,2)
        # per_box=np.array(per_box,np.int32).reshape(-1,1,2)
        cv2.polylines(im,[ori_array],1,(255,0,0),3)
        # plt.imshow(im)
        # plt.show()
    for j in range(len(list_det_box)):
        ori_array=np.array(list_det_box[j],np.int32).reshape(-1,1,2)
        # per_array=np.array(per_array,np.int32).reshape(-1,1,2)
        # res_box=np.array(res_box,np.int32).reshape(-1,1,2)
        # per_box=np.array(per_box,np.int32).reshape(-1,1,2)
        cv2.polylines(im,[ori_array],1,(0,255,0),3)
    # cv2.polylines(im,[np.array([10,6,1377,15,1344,783,14,810],np.int32).reshape(-1,1,2)],1,(0,255,0),3)
    # plt.imshow(im)
    # plt.show()
    plt.imsave(save_name,im)

    list_result=[]
    list_result.append(len(list_per_box))#[0]
    list_result.append(len(list_det_box))#[1]
    list_result.append(sum)#[2]
    if(len(list_per_box)==0):
        list_result.append(-1)
    else:
        list_result.append(sum * 1.0 / len(list_per_box))  # [3]
    if(len(list_det_box)==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/len(list_det_box))#[4]
    if((len(list_per_box)+len(list_det_box))==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/(len(list_per_box)+len(list_det_box)))#[5]
    if((len(list_per_box)+len(list_det_box)-sum)==0):
        list_result.append(-1)
    else:
        list_result.append(sum*1.0/(len(list_per_box)+len(list_det_box)-sum))#[6]
    list_result.append(newTextFlag) #[7]
    return list_result

def get_detect_box(path,list_box):
    with open(path,'r')as file:
        for line in file.readlines():
            if line=="\n":
                continue
            num=line.strip('\n').split(',')
            # array_text_box=np.array(num,np.float32).reshape(-1,2)
            for i in range(len(num)):
                num[i]=int(num[i])
            list_box.append(num)
    # print("list_box\n"+str(list_box))
    pass

# def evaluate_light():
#     # PATH = "A:\\textRe\\0808\\save_test\\per_"
#     # detect 1~500
#     without_file=[]
#     PATH_WITHOUTFILE="A:\\textRe\\0901\\withoutfile_wm.txt"
#     PATH_RES = "A:\\textRe\\0901\\ori-20190901T133949Z-001\\ori\\img_"
#         #"A:\\textRe\\0823\\ori_light (1)\\ori_light\\txt\\res_img_"
#         #"A:\\textRe\\0808\\submit_ic15 (2)\\res_img_"
#     PATH_DET = "A:\\textRe\\0901\\txt_7\\img_"
#         #"A:\\textRe\\0808\\submit_ic15\\res_img_"
#     # "A:\\textRe\\0808\\submit_ic15-20190812T113442Z-001\\submit_ic15\\res_img_"191-300
#     # "A:\\textRe\\0808\\submit_ic15\\res_img_" #first 1-42
#     PATH_LIGHT_AVERAGE = "A:\\textRe\\0901\\light_score\\average_"
#     PATH_SAVE_IMG = "A:\\textRe\\0901\\light_img\\light_"
#     # PATH_ALL_AVERAGE = "A:\\textRe\\0808\\all_average.txt"
#     PATH_ALL_LIGHT_AVERAGE="A:\\textRe\\0901\\all_average_light.txt"
#     border = 50
#     width = 1280
#     height = 720
#     outsourcing_enclosure = [[border, border], [border + width, border], [border + width, height + border],
#                              [border, height + border]]
#
#     for i in range(1, 501, 1):
#         pass
#         # path = PATH + str(i) + ".txt"
#         path_res = PATH_RES + str(i) + ".txt"
#
#         # cal list_box ,get dectect i in range(1~500)
#         list_box = []
#         if os.path.exists(path_res):
#             get_detect_box(path_res, list_box)
#         else:
#             without_file.append(i)
#         # print(list_box)
#
#         list_all_file_result = []
#         for j in range(5, 101, 5):  # len(list_M)):
#             # cal per box
#             path_det = PATH_DET + str(i * 1000 + j+100) + ".txt"
#             path_average = PATH_LIGHT_AVERAGE + str(i) + ".txt"
#             save_name = PATH_SAVE_IMG + str(i) + "_" + str(j) + ".jpg"
#             list_per_box = copy.deepcopy(list_box)
#             # M = list_M[j - 1]
#
#             # newborder = cal_two_per(M, outsourcing_enclosure)
#
#             # cal_per_box(M, list_box, list_per_box)
#             # get 变换后的图片检测后的list
#             list_det_box = []
#             get_detect_box(path_det, list_det_box)
#             # print("iou")
#             # 得到iou
#             single_file_result = cal_box_iou(list_per_box, list_det_box, [], save_name)
#             # print("single_file_result: "+str(single_file_result))
#             list_all_file_result.append(single_file_result)
#             print("list_all_file_result    i=" + str(i) + "    j=" + str(j))
#             print(single_file_result)
#         # cal all average
#         list_average = [0, 0, 0, 0,0]
#         for m in range(len(list_all_file_result)):
#             list_average[0] += list_all_file_result[m][3]
#             list_average[1] += list_all_file_result[m][4]
#             list_average[2] += list_all_file_result[m][5]
#             list_average[3] += list_all_file_result[m][6]
#             list_average[4] += list_all_file_result[m][7]
#         for n in range(len(list_average)):
#             list_average[n] *= 1.0
#             list_average[n] /= len(list_all_file_result)
#         print("list_average    i=" + str(i))
#         print(list_average)
#         # save all file
#         with open(path_average, 'w')as file:
#             for m in range(len(list_all_file_result)):
#                 file.write(str(i) + " " + str(m + 1))
#                 for n in range(len(list_all_file_result[m])):
#                     file.write(" " + str(list_all_file_result[m][n]))
#                 file.write("\n")
#         with open(PATH_ALL_LIGHT_AVERAGE, 'a')as file:
#             file.write(str(i))
#             for m in range(len(list_average)):
#                 file.write(" " + str(list_average[m]))
#             file.write("\n")

def evaluate_watermark():
    without_file=[]
    PATH_WITHOUTFILE="A:\\textRe\\0901\\withoutfile_wm.txt"
    PATH_RES = "A:\\textRe\\0901\\ori-20190901T133949Z-001\\ori\\img_"
    #"A:\\textRe\\0823\\ori_light (1)\\ori_light\\txt\\res_img_"
    #"A:\\textRe\\0808\\submit_ic15 (2)\\res_img_"
    PATH_DET = "A:\\textRe\\0901\\txt11\\img_"
    # PATH = "A:\\textRe\\0808\\save_test\\per_"
    # detect 1~500
    # PATH_RES = "A:\\textRe\\0901\\ori_light (1)\\ori_light\\txt\\res_img_"
    # detect 1305~130...
    # PATH_DET = "A:\\textRe\\0823\\model.ckpt-73018\\txt\\res_img_"
    # "A:\\textRe\\0823\\submit_ic15 (6)\\res_img_"
    # "A:\\textRe\\0808\\submit_ic15-20190812T113442Z-001\\submit_ic15\\res_img_"191-300
    # "A:\\textRe\\0808\\submit_ic15\\res_img_" #first 1-42
    # PATH_LIGHT_AVERAGE = "A:\\textRe\\0808\\light_score\\average_"
    PATH_WM_AVERAGE="A:\\textRe\\0901\\wm_score\\average_"
    PATH_SAVE_IMG = "A:\\textRe\\0901\\wm_img\\wm_"
    PATH_WM_ADD_TEXT=""
        #"A:\\textRe\\0808\\light_img\\light_"
    # PATH_ALL_AVERAGE = "A:\\textRe\\0808\\all_average.txt"
    PATH_ALL_WM_AVERAGE = "A:\\textRe\\0901\\all_average_wm.txt"
    PATH_TEXT_POS="A:\\textRe\\0808\\drive-download-20190820T060904Z-001\\wm_text_pos\\img_"
    border = 50
    width = 1280
    height = 720
    outsourcing_enclosure = [[border, border], [border + width, border], [border + width, height + border],
                             [border, height + border]]

    for i in range(251, 501, 1):
        pass
        # path = PATH + str(i) + ".txt"
        path_res = PATH_RES + str(i) + ".txt"
        path_text_pos=PATH_TEXT_POS+str(i)+".txt"

        # cal list_box ,get dectect i in range(1~500)
        list_box = []
        if os.path.exists(path_res):
            get_detect_box(path_res, list_box)
        else:
            without_file.append(i)
        # get_detect_box(path_res, list_box)

        list_text_pos=[]
        get_detect_box(path_text_pos, list_text_pos)
        # print(list_box)

        list_all_file_result = []
        for j in range(1, 21, 1):  # len(list_M)):
            # cal per box
            path_det = PATH_DET + str(i * 1000 + j + 300) + ".txt"
            path_average = PATH_WM_AVERAGE + str(i) + ".txt"
            save_name = PATH_SAVE_IMG + str(i) + "_" + str(j) + ".jpg"
            list_per_box = copy.deepcopy(list_box)
            # M = list_M[j - 1]

            # newborder = cal_two_per(M, outsourcing_enclosure)

            # cal_per_box(M, list_box, list_per_box)
            # get 变换后的图片检测后的list

            list_det_box = []
            if os.path.exists(path_det):
                get_detect_box(path_det, list_det_box)
                # continue
            else:
                without_file.append(i * 1000 + j + 300)
                continue
            # print("iou")
            # 得到iou
            single_file_result = cal_box_iou(list_per_box, list_det_box, [], save_name,list_text_pos[j-1])
            # print("single_file_result: "+str(single_file_result))
            list_all_file_result.append(single_file_result)
            print("list_all_file_result    i=" + str(i) + "    j=" + str(j))
            print(single_file_result)
        # cal all average
        list_average = [0, 0, 0, 0,0]
        for m in range(len(list_all_file_result)):
            list_average[0] += list_all_file_result[m][3]
            list_average[1] += list_all_file_result[m][4]
            list_average[2] += list_all_file_result[m][5]
            list_average[3] += list_all_file_result[m][6]
            list_average[4] += list_all_file_result[m][7]
        for n in range(len(list_average)):
            list_average[n] *= 1.0
            if len(list_all_file_result)!=0:
                list_average[n] /= len(list_all_file_result)
            else:
                list_average[n]=-1
        print("list_average    i=" + str(i))
        print(list_average)
        # save all file
        with open(path_average, 'w')as file:
            for m in range(len(list_all_file_result)):
                file.write(str(i) + " " + str(m + 1))
                for n in range(len(list_all_file_result[m])):
                    file.write(" " + str(list_all_file_result[m][n]))
                file.write("\n")
        with open(PATH_ALL_WM_AVERAGE, 'a')as file:
            file.write(str(i))
            for m in range(len(list_average)):
                file.write(" " + str(list_average[m]))
            file.write("\n")
    with open(PATH_WITHOUTFILE,'a')as file:
        for m in range(len(without_file)):
            file.write(str(without_file[m])+"\n")

if __name__ == '__main__':
    # test_cal_two_per()
    # evaluate_large()
    # evaluate_light()
    evaluate_watermark()