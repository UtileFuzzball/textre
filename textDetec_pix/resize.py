#encoding=utf-8
#author: walker
#date: 2014-05-15
#function: 更改图片尺寸大小
import os
import os.path
from PIL import Image
'''
filein: 输入图片
fileout: 输出图片
width: 输出图片宽度
height:输出图片高度
type:输出图片类型（png, gif, jpeg...）
'''
def ResizeImage(filein, fileout, width, height, type):
    img = Image.open(filein)
    out = img.resize((width, height),Image.ANTIALIAS) #resize image with high-quality
    out.save(fileout, type)
if __name__ == "__main__":

    PATH_IMG="A:\\textRe\\change_test\\img_"
    PATH_SAVE_IMG="A:\\textRe\\save_test\\resize_img\\img_"
    for i in range(1,4,1):
        path_img = PATH_IMG+str(i)+".jpg"
        print("i="+str(i))
        for j in range(1,11,1):
            path_save_img = PATH_SAVE_IMG+str(i*1000+400+j)+".jpg"
            width = int(1280*(1+j/10))
            # print(width)
            height = 720
            type = 'jpeg'
            ResizeImage(path_img, path_save_img, width, height, type)
        for j in range(1,11,1):
            path_save_img = PATH_SAVE_IMG+str(i*1000+500+j)+".jpg"
            width = 1280
            # print(width)
            height = int(720*(1+j/10))
            type = 'jpeg'
            ResizeImage(path_img, path_save_img, width, height, type)